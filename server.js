var http = require('http');
var url = require('url');
var fs = require('fs');
const port = 9595
var server = http.createServer(function(request, response) {
    var path = url.parse(request.url).pathname;
    switch (path) {

        case '/getArticles':
            fs.readFile(__dirname + "/db/articles.json", function(error, data) {
                data = JSON.parse(data);
                if (error) {
                    response.writeHead(404);
                    response.write(error);
                    response.end();
                } else {
                    response.writeHead(200, { 'Content-Type': 'text/plain' });
                    response.write(JSON.stringify(data));
                    response.end();
                }
            });
            break;

        case '/saveArticle':
            request.on('data', chunk => {
                let act = JSON.parse(chunk.toString());
                fs.readFile(__dirname + "/db/articles.json", function(error, data) {
                    data = JSON.parse(data);
                    if (error) {
                        response.writeHead(404);
                        response.write(error);
                        response.end();
                    } else {
                        let last = data[data.length - 1];
                        act.id = last ? last.id + 1 : 0;
                        data.push(act);
                        fs.writeFile(__dirname + "/db/articles.json", JSON.stringify(data), error => {
                            if (error) {
                                response.writeHead(404);
                                response.write(error);
                                response.end();
                            } else {
                                response.writeHead(200, { 'Content-Type': 'text/plain' });
                                response.write(JSON.stringify(act));
                                response.end();
                            }
                        });
                    }
                });
            });
            break;

        case '/addComment':
            request.on('data', chunk => {
                let comm = JSON.parse(chunk.toString());
                fs.readFile(__dirname + "/db/articles.json", function(error, data) {
                    data = JSON.parse(data);
                    if (error) {
                        response.writeHead(404);
                        response.write(error);
                        response.end();
                    } else {
                        data.map(act => {
                            if (act.id === comm.actId) {
                                act.comments.push(comm);
                                return act;
                            } else {
                                return act;
                            }
                        });
                        fs.writeFile(__dirname + "/db/articles.json", JSON.stringify(data), error => {
                            if (error) {
                                response.writeHead(404);
                                response.write(error);
                                response.end();
                            } else {
                                response.writeHead(200, { 'Content-Type': 'text/plain' });
                                response.write(JSON.stringify(comm));
                                response.end();
                            }
                        });
                    }
                });
            });
            break;

        case '/login':
            request.on('data', chunk => {
                let credentials = JSON.parse(chunk.toString());
                let answer = { error: true };
                fs.readFile(__dirname + "/db/users.json", function(error, data) {
                    data = JSON.parse(data);
                    if (error) {
                        response.writeHead(404);
                        response.write(error);
                        response.end();
                    } else {
                        data.forEach(user => {
                            if (user.username === credentials.username && user.password === credentials.password) {
                                answer = { username: user.username, error: false };
                            }
                        });
                        response.writeHead(200, { 'Content-Type': 'text/plain' });
                        response.write(JSON.stringify(answer));
                        response.end();
                    }
                });
            });
            break;

        case '/favicon.ico':
            response.write("Icon");
            response.end();
            break;

        case '':
        case '/':
            path = '/assets/index.html'
        default:
            fs.readFile(__dirname + path, function(error, data) {
                if (error) {
                    response.writeHead(404);
                    response.write(error);
                    response.end();
                } else {
                    response.write(data);
                    response.end();
                }
            });
            break;
    }
});
server.listen(port, error => {
    if (error) {
        console.log("SOMETHING WENT WRONG")
    } else {
        console.log("SERVER STARTED AT PORT :: " + port);
    }
});