module Login where

import Prelude
import Affjax (post)
import Affjax.RequestBody as RB
import Affjax.ResponseFormat (json)
import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, encodeJson, (.!=), (.:), (.:?), (:=), (~>))
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (class_)
import User (User)

type State
  = { username :: String
    , password :: String
    , error :: Boolean
    }

newtype LoginJSON
  = LoginJSON State

instance decodeJsonLogin :: DecodeJson LoginJSON where
  decodeJson json = do
    obj <- decodeJson json
    username <- obj .:? "username" .!= ""
    error <- obj .: "error"
    pure $ LoginJSON { username: username, password: "", error: error }

instance encodeJsonLogin :: EncodeJson LoginJSON where
  encodeJson (LoginJSON info) =
    "member" := info.username
      ~> "password"
      := info.password

type Slot
  = H.Slot Query Output

data Action
  = Out Output
  | UsernameInput String
  | PasswordInput String
  | Validate

data Query a
  = Unit

data Output
  = LoggedIn User

component :: forall a m. MonadAff m => H.Component HH.HTML Query a Output m
component =
  H.mkComponent
    { initialState: \_ -> { username: "", password: "", error: false }
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  HH.div
    [ class_ "auth-page" ]
    [ HH.div
        [ class_ "container page" ]
        [ HH.div
            [ class_ "row" ]
            [ HH.div
                [ class_ "col-md-6 offset-md-3 col-xs12" ]
                [ HH.h1 [ class_ "text-xs-center" ] [ HH.text "Log in" ]
                , HH.fieldset_
                    [ label
                    , HH.fieldset [ class_ "form-group" ]
                        [ HH.input
                            [ class_ "form-control form-control-lg"
                            , HHP.placeholder "Username"
                            , HHP.value state.username
                            , HHP.type_ HHP.InputText
                            , HHE.onValueInput $ Just <<< UsernameInput
                            ]
                        ]
                    , HH.fieldset [ class_ "form-group" ]
                        [ HH.input
                            [ class_ "form-control form-control-lg"
                            , HHP.placeholder "Password"
                            , HHP.value state.password
                            , HHP.type_ HHP.InputPassword
                            , HHE.onValueInput $ Just <<< PasswordInput
                            ]
                        ]
                    , HH.button
                        [ class_ "btn btn-lg btn-primary"
                        , HHE.onClick \_ -> Just Validate
                        ]
                        [ HH.text "Log in" ]
                    ]
                ]
            ]
        ]
    ]
  where
  label =
    if state.error then
      HH.div
        [ class_ "error-messages" ]
        [ HH.text "Username or password is invalid" ]
    else
      HH.text ""

handleAction :: forall m. MonadAff m => Action -> H.HalogenM State Action () Output m Unit
handleAction = case _ of
  Out output -> case output of
    LoggedIn user -> H.raise $ LoggedIn user
  UsernameInput val -> H.modify_ (_ { username = val })
  PasswordInput val -> H.modify_ (_ { password = val })
  Validate -> do
    state <- H.get
    response <- H.liftAff $ post json "/login" $ RB.json $ encodeJson state
    let
      result = case response.body of
        Left err -> { username: "", password: "", error: true }
        Right json -> case decodeJson json of
          Left err -> { username: "", password: "", error: true }
          Right (LoginJSON info) -> info
    if result.error then
      H.modify_ (\st -> result)
    else
      void $ H.fork $ handleAction $ Out $ LoggedIn { username: result.username }
