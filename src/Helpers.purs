module Helpers where

import Prelude
import Data.Array (filter, length)
import Data.Formatter.DateTime (FormatterCommand(..), format)
import Data.Int (decimal, toStringAs, fromStringAs)
import Data.List (List, fromFoldable)
import Data.Maybe (Maybe(..))
import Data.String (Pattern(..), joinWith, split)
import Effect (Effect)
import Effect.Now (nowDateTime)
import Effect.Unsafe (unsafePerformEffect)
import Halogen.HTML as HH
import Halogen.HTML.Properties as HHP
import Web.HTML (window)
import Web.HTML.Window (localStorage)
import Web.Storage.Storage (getItem, setItem)

class_ :: forall r i. String -> HH.IProp ( class :: String | r ) i
class_ name = (HH.ClassName >>> HHP.class_) name

today :: String
today = format dateFormat $ unsafePerformEffect nowDateTime

dateFormat :: List FormatterCommand
dateFormat =
  fromFoldable
    [ DayOfWeekNameShort
    , (Placeholder ", ")
    , MonthShort
    , (Placeholder " ")
    , DayOfMonthTwoDigits
    , (Placeholder ". ")
    , YearFull
    ]

tokenId :: String
tokenId = "likedArticles"

readFromStorage :: Effect (Array Int)
readFromStorage = do
  str <- getItem tokenId =<< localStorage =<< window
  pure
    $ case str of
        Nothing -> []
        Just value -> (\id -> fromString id) <$> split (Pattern ",") value

writeToStorage :: Int -> Effect Unit
writeToStorage id = do
  storage <- getItem tokenId =<< localStorage =<< window
  let
    added = case storage of
      Nothing -> (toString id) <> ","
      Just value -> value <> (toString id) <> ","
  setItem tokenId added =<< localStorage =<< window

removeFromStorage :: Int -> Effect Unit
removeFromStorage id = do
  storage <- getItem tokenId =<< localStorage =<< window
  let
    removed = case storage of
      Nothing -> []
      Just value -> (\token -> token /= id && token /= -1) `filter` fromStringArray value
  setItem tokenId (toStringArray removed) =<< localStorage =<< window

toString :: Int -> String
toString int = toStringAs decimal int

toStringArray :: Array Int -> String
toStringArray array = joinWith " " $ (\int -> (toString int) <> ",") <$> array

fromString :: String -> Int
fromString string = case fromStringAs decimal string of
  Nothing -> -1
  Just value -> value

fromStringArray :: String -> Array Int
fromStringArray string = (\id -> fromString id) <$> split (Pattern ",") string

contains :: forall a. Eq a => Array a -> a -> Boolean
contains array value =
  let
    filtered = (\val -> val == value) `filter` array
  in
    length filtered > 0
