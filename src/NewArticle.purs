module NewArticle where

import Article (Article, ArticleJSON(..), init)
import Prelude
import Affjax (post)
import Affjax.RequestBody as RB
import Affjax.ResponseFormat (json)
import Data.Argonaut (decodeJson, encodeJson)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (class_, today)
import User (User)

type Slot
  = H.Slot Query Output

data Action
  = Receive Input
  | Out Output
  | HeadlineInput String
  | DescriptionInput String
  | TextInput String
  | AddArticle

data Query a
  = Unit

type Input
  = { user :: Maybe User }

data Output
  = Add Article

username :: Input -> String
username input = case input.user of
  Nothing -> ""
  Just u -> u.username

component :: forall m. MonadAff m => Input -> H.Component HH.HTML Query Input Output m
component input =
  H.mkComponent
    { initialState: \_ -> init { author = username input, date = today }
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , receive = Just <<< Receive
            }
    }

render :: forall m. Article -> H.ComponentHTML Action () m
render state =
  HH.div
    [ class_ "auth-page" ]
    [ HH.div
        [ class_ "container page" ]
        [ HH.div
            [ class_ "row" ]
            [ HH.div
                [ class_ "col-md-6 offset-md-3 col-xs12" ]
                [ HH.h1 [ class_ "text-xs-center" ] [ HH.text "New article" ]
                , HH.fieldset_
                    [ HH.fieldset [ class_ "form-group" ]
                        [ HH.input
                            [ class_ "form-control form-control-lg"
                            , HHP.placeholder "Headline"
                            , HHP.type_ HHP.InputText
                            , HHE.onValueInput $ Just <<< HeadlineInput
                            ]
                        ]
                    , HH.fieldset [ class_ "form-group" ]
                        [ HH.textarea
                            [ class_ "form-control form-control-lg"
                            , HHP.placeholder "Description"
                            , HHP.rows 2
                            , HHE.onValueInput $ Just <<< DescriptionInput
                            ]
                        ]
                    , HH.fieldset [ class_ "form-group" ]
                        [ HH.textarea
                            [ class_ "form-control form-control-lg"
                            , HHP.placeholder "Text"
                            , HHP.rows 5
                            , HHE.onValueInput $ Just <<< TextInput
                            ]
                        ]
                    , HH.button
                        [ class_ "btn btn-lg btn-primary"
                        , HHE.onClick \_ -> Just AddArticle
                        ]
                        [ HH.text "Add" ]
                    ]
                ]
            ]
        ]
    ]

handleAction :: forall m. MonadAff m => Action -> H.HalogenM Article Action () Output m Unit
handleAction = case _ of
  Receive input -> H.modify_ (_ { author = username input, date = today })
  Out output -> case output of
    Add state -> H.raise $ Add state
  HeadlineInput val -> H.modify_ (_ { headline = val })
  DescriptionInput val -> H.modify_ (_ { description = val })
  TextInput val -> H.modify_ (_ { text = val })
  AddArticle -> do
    state <- H.get
    response <- H.liftAff $ post json "/saveArticle" $ RB.json $ encodeJson state
    let
      result = case response.body of
        Left err -> init { id = -1 }
        Right json -> case decodeJson json of
          Left err -> init { id = -1 }
          Right (ArticleJSON act) -> act
    if result.id < 0 then
      H.modify_ (\st -> result)
    else
      void $ H.fork $ handleAction $ Out $ Add result
