module Article where

import Prelude
import Comment (Comment)
import Comment as Comment
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, decodeJson, encodeJson, fromArray, (.:), (:=), (~>))
import Data.Array (snoc)
import Data.Either (Either)
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Data.Traversable (traverse)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (class_)

type Article
  = { id :: Int
    , author :: String
    , headline :: String
    , description :: String
    , text :: String
    , date :: String
    , liked :: Boolean
    , comments :: Array Comment
    }

newtype ArticleJSON
  = ArticleJSON Article

instance decodeJsonArticle :: DecodeJson ArticleJSON where
  decodeJson json = do
    obj <- decodeJson json
    id <- obj .: "id"
    author <- obj .: "author"
    headline <- obj .: "headline"
    description <- obj .: "description"
    text <- obj .: "text"
    date <- obj .: "date"
    comments <- obj .: "comments"
    pure $ ArticleJSON { id: id, author: author, headline: headline, description: description, text: text, date: date, liked: false, comments: comments }

instance encodeJsonArticle :: EncodeJson ArticleJSON where
  encodeJson (ArticleJSON act) =
    "id" := act.id
      ~> "author"
      := act.author
      ~> "headline"
      := act.headline
      ~> "description"
      := act.description
      ~> "text"
      := act.text
      ~> "date"
      := act.date

type ArticleArray
  = Array ArticleJSON

decodeArticleArray :: Json -> Either String ArticleArray
decodeArticleArray json = decodeJson json >>= traverse decodeJson

encodeArticleArray :: ArticleArray -> Json
encodeArticleArray acta = fromArray $ encodeJson <$> acta

type Slot
  = H.Slot Query Output

type ChildSlots
  = ( comment :: Comment.Slot Unit
    )

data Action
  = Out Output
  | CommentOutput Comment.Output

data Query a
  = Unit

data Output
  = Favorite Int
  | Unfavorite Int
  | AddComment Int Comment

component :: forall m. MonadAff m => Article -> H.Component HH.HTML Query Unit Output m
component act =
  H.mkComponent
    { initialState: \_ -> act
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            }
    }

init :: Article
init =
  { id: 0
  , author: ""
  , headline: ""
  , description: ""
  , text: ""
  , date: ""
  , liked: false
  , comments: []
  }

render :: forall m. MonadAff m => Article -> H.ComponentHTML Action ChildSlots m
render state =
  HH.div
    [ class_ "article-page" ]
    [ HH.div
        [ class_ "banner" ]
        [ HH.div
            [ class_ "container" ]
            [ HH.h1_
                [ HH.text state.headline ]
            , HH.div
                [ class_ "article-meta" ]
                [ HH.a_
                    [ HH.img
                        [ HHP.src "../assets/images/avatar.jpg" ]
                    ]
                , HH.div
                    [ class_ "info" ]
                    [ HH.a
                        [ class_ "author"
                        ]
                        [ HH.text state.author ]
                    , HH.span
                        [ class_ "date" ]
                        [ HH.text state.date ]
                    ]
                , HH.span_
                    [ HH.button
                        [ class_ $ "btn btn-sm " <> if state.liked then "btn-primary" else "btn-outline-primary"
                        , HHE.onClick \_ -> if state.liked then Just $ Out $ Unfavorite state.id else Just $ Out $ Favorite state.id
                        ]
                        [ HH.i
                            [ class_ "ion-heart" ]
                            []
                        , HH.span_
                            [ if state.liked then HH.text " Unfavorite Article" else HH.text " Favorite Article"
                            ]
                        ]
                    ]
                ]
            ]
        ]
    , HH.div
        [ class_ "container page" ]
        [ HH.div
            [ class_ "col-xs-12" ]
            [ HH.div
                [ class_ "article-text-wrap" ]
                [ HH.p_ [ HH.text state.text ] ]
            , HH.hr_
            , HH.div
                [ class_ "article-actions" ]
                [ HH.div
                    [ class_ "article-meta" ]
                    [ HH.a_
                        [ HH.img
                            [ HHP.src "../assets/images/avatar.jpg" ]
                        ]
                    , HH.div
                        [ class_ "info" ]
                        [ HH.a
                            [ class_ "author"
                            ]
                            [ HH.text state.author ]
                        , HH.span
                            [ class_ "date" ]
                            [ HH.text state.date ]
                        ]
                    , HH.span_
                        [ HH.button
                            [ class_ $ "btn btn-sm " <> if state.liked then "btn-primary" else "btn-outline-primary"
                            , HHE.onClick \_ -> if state.liked then Just $ Out $ Unfavorite state.id else Just $ Out $ Favorite state.id
                            ]
                            [ HH.i
                                [ class_ "ion-heart" ]
                                []
                            , HH.span_
                                [ if state.liked then HH.text " Unfavorite Article" else HH.text " Favorite Article"
                                ]
                            ]
                        ]
                    ]
                ]
            , HH.div
                [ class_ "row" ]
                [ HH.div
                    [ class_ "col-xs-12 col-md-8 offset-md-2" ]
                    $ (\comment -> viewComment comment)
                    <$> state.comments
                ]
            , HH.slot (SProxy :: SProxy "comment") unit (Comment.component { id: state.id }) unit (Just <<< CommentOutput)
            ]
        ]
    ]
  where
  viewComment comment =
    HH.div
      [ class_ "card" ]
      [ HH.div
          [ class_ "card-block" ]
          [ HH.p
              [ class_ "card-text" ]
              [ HH.text comment.text ]
          ]
      , HH.div
          [ class_ "card-footer" ]
          [ HH.a
              [ class_ "comment-author"
              ]
              [ HH.img
                  [ class_ "comment-author-img"
                  , HHP.src "../assets/images/avatar.jpg"
                  ]
              ]
          , HH.text " "
          , HH.a
              [ class_ "comment-author"
              ]
              [ HH.text comment.author ]
          , HH.text " "
          , HH.span
              [ class_ "date-posted" ]
              [ HH.text comment.date ]
          ]
      ]

handleAction :: forall m. Action -> H.HalogenM Article Action ChildSlots Output m Unit
handleAction = case _ of
  Out output -> case output of
    Favorite id -> do
      H.modify_ (_ { liked = true })
      H.raise $ Favorite id
    Unfavorite id -> do
      H.modify_ (_ { liked = false })
      H.raise $ Unfavorite id
    AddComment id comment -> H.raise $ AddComment id comment
  CommentOutput out -> case out of
    Comment.Add comment -> do
      state <- H.get
      H.modify_ (_ { comments = snoc state.comments comment })
      void $ H.fork $ handleAction $ Out $ AddComment state.id comment

react :: Array Article -> Int -> Boolean -> Array Article
react array id reaction =
  ( \act ->
      if act.id == id then
        act { liked = reaction }
      else
        act
  )
    <$> array
