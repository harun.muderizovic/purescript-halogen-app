module Comment where

import Prelude
import Affjax (post)
import Affjax.RequestBody as RB
import Affjax.ResponseFormat (json)
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, decodeJson, encodeJson, fromArray, (.:), (:=), (~>))
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Traversable (traverse)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (today, class_)

type Comment
  = { actId :: Int
    , text :: String
    , author :: String
    , date :: String
    }

newtype CommentJSON
  = CommentJSON Comment

instance decodeJsonArticle :: DecodeJson CommentJSON where
  decodeJson json = do
    obj <- decodeJson json
    actId <- obj .: "actId"
    text <- obj .: "text"
    author <- obj .: "author"
    date <- obj .: "date"
    pure $ CommentJSON { actId: actId, text: text, author: author, date: date }

instance encodeJsonArticle :: EncodeJson CommentJSON where
  encodeJson (CommentJSON comm) =
    "actId" := comm.actId
      ~> "text"
      := comm.text
      ~> "author"
      := comm.author
      ~> "date"
      := comm.date

type CommentArray
  = Array CommentJSON

decodeCommentArray :: Json -> Either String CommentArray
decodeCommentArray json = decodeJson json >>= traverse decodeJson

encodeCommentArray :: CommentArray -> Json
encodeCommentArray comma = fromArray $ encodeJson <$> comma

type Slot
  = H.Slot Query Output

data Action
  = NameInput String
  | TextInput String
  | AddBtn
  | Out Output

type Input
  = { id :: Int }

data Output
  = Add Comment

data Query a
  = Unit

component :: forall a m. MonadAff m => Input -> H.Component HH.HTML Query a Output m
component input =
  H.mkComponent
    { initialState: \_ -> { actId: input.id, text: "", author: "", date: "" }
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            }
    }

render :: forall m. Comment -> H.ComponentHTML Action () m
render state =
  HH.fieldset
    [ class_ "col-xs-12 col-md-8 offset-md-2" ]
    [ HH.fieldset [ class_ "form-group" ]
        [ HH.input
            [ class_ "form-control form-control-lg"
            , HHP.placeholder "Name"
            , HHP.type_ HHP.InputText
            , HHP.value state.author
            , HHE.onValueInput $ Just <<< NameInput
            ]
        ]
    , HH.fieldset [ class_ "form-group" ]
        [ HH.textarea
            [ class_ "form-control form-control-lg"
            , HHP.placeholder "Comment"
            , HHP.rows 3
            , HHP.value state.text
            , HHE.onValueInput $ Just <<< TextInput
            ]
        ]
    , HH.button
        [ class_ "btn btn-lg btn-primary"
        , HHE.onClick \_ -> Just AddBtn
        ]
        [ HH.text "Add" ]
    ]

handleAction :: forall m. MonadAff m => Action -> H.HalogenM Comment Action () Output m Unit
handleAction = case _ of
  Out output -> case output of
    Add comment -> H.raise $ Add comment
  NameInput value -> H.modify_ (_ { author = value })
  TextInput value -> H.modify_ (_ { text = value })
  AddBtn -> do
    state <- H.get
    response <- H.liftAff $ post json "/addComment" $ RB.json $ encodeJson $ state { date = today }
    let
      result = case response.body of
        Left err -> state { actId = -1 }
        Right json -> case decodeJson json of
          Left err -> state { actId = -1 }
          Right (CommentJSON comm) -> comm
    if result.actId < 0 then
      H.modify_ (\st -> result)
    else do
      void $ H.fork $ handleAction $ Out $ Add result
      H.modify_ (\st -> st { text = "", author = "", date = "" })
