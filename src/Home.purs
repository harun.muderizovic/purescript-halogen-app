module Home where

import Prelude
import Article (Article)
import Data.Array (reverse)
import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (class_)
import User (User)

type State
  = { articles :: Array Article
    , user :: Maybe User
    }

type Slot
  = H.Slot Query Output

data Action
  = Receive State
  | Out Output

data Query a
  = Unit

data Output
  = OpenArticle Article

component :: forall m. State -> H.Component HH.HTML Query State Output m
component state =
  H.mkComponent
    { initialState: \_ -> state
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , receive = Just <<< Receive
            }
    }

render :: forall m. State -> H.ComponentHTML Action () m
render state =
  HH.div
    [ class_ "home-page" ]
    [ HH.div
        [ class_ "banner" ]
        [ HH.div
            [ class_ "container" ]
            [ HH.h1
                [ class_ "logo-font" ]
                [ HH.text "Halogen" ]
            , HH.p_
                [ HH.text "A PureScript example app." ]
            ]
        ]
    , HH.div
        [ class_ "container page" ]
        [ HH.div
            [ class_ "row" ]
            [ HH.div
                [ class_ "col-md-12" ]
                [ HH.div_
                    $ (\article -> renderArticle article)
                    <$> reverse state.articles
                ]
            ]
        ]
    ]

renderArticle :: forall a. Article -> HH.HTML a Action
renderArticle act =
  HH.div
    [ class_ "article-preview" ]
    [ HH.div
        [ class_ "article-meta" ]
        [ HH.a
            []
            [ HH.img
                [ HHP.src "../assets/images/avatar.jpg"
                ]
            ]
        , HH.div
            [ class_ "info" ]
            [ HH.a
                [ class_ "author" ]
                [ HH.text act.author ]
            , HH.span
                [ class_ "date" ]
                [ HH.text act.date ]
            ]
        , HH.div
            [ class_ "pull-xs-right" ]
            [ HH.button
                [ class_ $ "passive btn btn-sm " <> if act.liked then "btn-primary" else "btn-outline-primary"
                ]
                [ HH.i
                    [ class_ "ion-heart" ]
                    []
                ]
            ]
        ]
    , HH.a
        [ class_ "preview-link", HHE.onClick \_ -> Just $ Out $ OpenArticle act ]
        [ HH.h1_
            [ HH.text act.headline ]
        , HH.p_
            [ HH.text act.description ]
        , HH.span_
            [ HH.text "Read more..." ]
        ]
    ]

handleAction :: forall m. Action -> H.HalogenM State Action () Output m Unit
handleAction = case _ of
  Receive state -> H.modify_ (\st -> state)
  Out output -> case output of
    OpenArticle act -> H.raise $ OpenArticle act
