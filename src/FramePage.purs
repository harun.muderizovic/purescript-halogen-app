module FramePage where

import Prelude
import Affjax (get)
import Affjax.ResponseFormat (json)
import Article (Article, ArticleJSON(..), decodeArticleArray, react)
import Article as Article
import Comment (Comment)
import Data.Array (snoc)
import Data.Const (Const)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Effect.Aff.Class (class MonadAff)
import Halogen (liftEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HHE
import Halogen.HTML.Properties as HHP
import Helpers (class_, contains, readFromStorage, removeFromStorage, writeToStorage)
import Home as Home
import Login as Login
import NewArticle as NewArticle
import User (User)

type State
  = { currentPage :: Page
    , currentUser :: Maybe User
    , articles :: Array Article
    }

type ChildSlots
  = ( home :: Home.Slot Unit
    , login :: Login.Slot Unit
    , article :: Article.Slot Unit
    , new_article :: NewArticle.Slot Unit
    )

data Action
  = Initialize
  | ChangePage Page
  | LogOut
  | HomeOutput Home.Output
  | LoginOutput Login.Output
  | ArticleOutput Article.Output
  | NewArticleOutput NewArticle.Output

data Query a
  = Unit a

data Page
  = Home
  | Login
  | ViewArticle Article.Article
  | NewArticle

derive instance pageEq :: Eq Page

component :: forall m. MonadAff m => H.Component HH.HTML (Const Void) Unit Void m
component =
  H.mkComponent
    { initialState: \_ -> { currentPage: Home, currentUser: Nothing, articles: [] }
    , render: render
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , initialize = Just Initialize
            }
    }

render :: forall m. MonadAff m => State -> H.ComponentHTML Action ChildSlots m
render state =
  HH.div_
    [ renderHeader state
    , renderContent state
    ]

renderHeader :: forall b. State -> HH.HTML b Action
renderHeader state =
  HH.nav
    [ class_ "navbar navbar-light" ]
    [ HH.div
        [ class_ "container" ]
        [ HH.a
            [ class_ "navbar-brand"
            , HHE.onClick \_ -> Just $ ChangePage Home
            ]
            [ HH.text "Halogen" ]
        , HH.ul
            [ class_ "nav navbar-nav pull-xs-right" ]
            $ [ navItem Home [ HH.text "Home" ]
              ]
            <> userSettings
        ]
    ]
  where
  userSettings = case state.currentUser of
    Just user ->
      [ navItem NewArticle [ HH.text "New article" ]
      , HH.li
          [ class_ "nav-item" ]
          [ HH.a
              [ class_ $ "nav-link"
              , HHE.onClick \_ -> Just LogOut
              ]
              [ HH.text "Log out" ]
          ]
      , HH.li
          [ class_ "nav-item user" ]
          [ HH.a
              [ class_ "nav-link" ]
              [ HH.text user.username ]
          ]
      , HH.li
          [ class_ "nav-item user-img" ]
          [ HH.a
              [ class_ "nav-link" ]
              [ HH.img
                  [ class_ "user-pic", HHP.src "../assets/images/avatar.jpg" ]
              ]
          ]
      ]
    Nothing -> [ navItem Login [ HH.text "Log in" ] ]

  navItem link html =
    HH.li
      [ class_ "nav-item" ]
      [ HH.a
          [ class_ $ "nav-link" <> if state.currentPage == link then " active" else ""
          , HHE.onClick \_ -> Just $ ChangePage link
          ]
          html
      ]

renderContent :: forall m. MonadAff m => State -> H.ComponentHTML Action ChildSlots m
renderContent state = case state.currentPage of
  Home -> HH.slot (SProxy :: SProxy "home") unit (Home.component { articles: state.articles, user: state.currentUser }) { articles: state.articles, user: state.currentUser } (Just <<< HomeOutput)
  ViewArticle article -> HH.slot (SProxy :: SProxy "article") unit (Article.component article) unit (Just <<< ArticleOutput)
  NewArticle -> HH.slot (SProxy :: SProxy "new_article") unit (NewArticle.component { user: state.currentUser }) { user: state.currentUser } (Just <<< NewArticleOutput)
  Login -> HH.slot (SProxy :: SProxy "login") unit Login.component unit (Just <<< LoginOutput)

handleAction :: forall m. MonadAff m => Action -> H.HalogenM State Action ChildSlots Void m Unit
handleAction = case _ of
  ChangePage page -> H.modify_ (_ { currentPage = page })
  LogOut -> H.modify_ (_ { currentPage = Login, currentUser = Nothing })
  HomeOutput out -> case out of
    Home.OpenArticle act -> H.modify_ (_ { currentPage = ViewArticle act })
  LoginOutput out -> case out of
    Login.LoggedIn user -> H.modify_ (_ { currentPage = Home, currentUser = Just user })
  ArticleOutput out -> case out of
    Article.Favorite id -> do
      storage <- liftEffect $ writeToStorage id
      H.modify_ (\state -> state { articles = react state.articles id true })
    Article.Unfavorite id -> do
      storage <- liftEffect $ removeFromStorage id
      H.modify_ (\state -> state { articles = react state.articles id false })
    Article.AddComment id comment -> H.modify_ (\state -> state { articles = addComment state.articles id comment })
  NewArticleOutput out -> case out of
    NewArticle.Add act -> do
      H.modify_ (\state -> state { articles = snoc state.articles act })
      void $ H.fork $ handleAction $ ChangePage Home
  Initialize -> do
    response <- H.liftAff $ get json "/getArticles"
    storage <- liftEffect $ readFromStorage
    H.modify_
      ( _
          { articles =
            case response.body of
              Left err -> []
              Right json -> case decodeArticleArray json of
                Left err -> []
                Right actArray ->
                  ( \(ArticleJSON act) -> do
                      if storage `contains` act.id then
                        act { liked = true }
                      else
                        act
                  )
                    <$> actArray
          }
      )

addComment :: Array Article -> Int -> Comment -> Array Article
addComment articles id comment =
  ( \article ->
      if article.id == id then
        article { comments = snoc article.comments comment }
      else
        article
  )
    <$> articles
