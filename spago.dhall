{ name =
    "purescript-halogen-example"
, sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
, packages =
    ./packages.dhall
, dependencies =
    [ "prelude"
    , "console"
    , "effect"
    , "variant"
    , "nonempty"
    , "aff"
    , "halogen"
    , "remotedata"
    , "routing"
    , "formatters"
    , "routing-duplex"
    , "now"
    , "affjax"
    , "slug"
    , "typelevel-prelude"
    , "argonaut-core"
    , "argonaut-codecs"
    , "aff-bus"
    , "struct"
    , "tolerant-argonaut"
    ]
}
